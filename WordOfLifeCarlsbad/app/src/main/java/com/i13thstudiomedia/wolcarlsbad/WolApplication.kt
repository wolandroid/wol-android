package com.i13thstudiomedia.wolcarlsbad

import android.app.Application
import androidx.lifecycle.LifecycleObserver

class WolApplication : Application(), LifecycleObserver  {
    override fun onCreate() {
        super.onCreate()
        instance = this
    }

    companion object {
        @get:Synchronized
        var instance: WolApplication? = null
            private set
    }
}