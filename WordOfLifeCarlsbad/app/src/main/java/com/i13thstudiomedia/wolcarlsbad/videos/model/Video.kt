package com.i13thstudiomedia.wolcarlsbad.videos.model

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Video {
    @SerializedName("id")
    @Expose
    var id: Int? = null

    @SerializedName("name")
    @Expose
    var name: String? = null

    @SerializedName("thumbnail")
    @Expose
    var thumbnail: String? = null

    @SerializedName("description")
    @Expose
    var description: String? = null

    @SerializedName("video_link")
    @Expose
    var videoLink: String? = null

}