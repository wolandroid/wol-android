package com.i13thstudiomedia.wolcarlsbad.videos

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.VolleyError
import com.google.gson.Gson
import com.i13thstudiomedia.wolcarlsbad.R
import com.i13thstudiomedia.wolcarlsbad.constants.Constants.Companion.VIDEOS_URL
import com.i13thstudiomedia.wolcarlsbad.constants.Constants.Companion.VIDEO_LINK
import com.i13thstudiomedia.wolcarlsbad.network.NetworkController
import com.i13thstudiomedia.wolcarlsbad.videos.adapter.FeatureAdapter
import com.i13thstudiomedia.wolcarlsbad.videos.adapter.VideosAdapter
import com.i13thstudiomedia.wolcarlsbad.videos.model.Video
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class VideosActivity : AppCompatActivity(), NetworkController.NetworkResponseListener,
    VideosAdapter.VideoItemListener, FeatureAdapter.VideoItemListener {
    var progressBar: ProgressBar? = null
    var mainView: View? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_videos)
        progressBar = findViewById(R.id.loader)
        mainView = findViewById<View>(R.id.main_view)
        NetworkController(this).addGetRequest(VIDEOS_URL, this)
        findViewById<ImageView>(R.id.closeButton).setOnClickListener {
            finish()
        }
    }

    override fun onError(error: VolleyError?) {
        Log.i("VidesR", "Error")
        progressBar?.visibility = View.GONE
        Toast.makeText(this, "somthing Went Wrong", Toast.LENGTH_SHORT).show()
    }


    override fun onResponse(response: Any?) {
        val featuredItems = ArrayList<Video>()
        val dailyContentItems = ArrayList<Video>()
        val classesItems = ArrayList<Video>()
        val messagesItems = ArrayList<Video>()
        try {
            val gson = Gson()
            val jsonObject = JSONObject(response.toString())
            val featureArray = jsonObject.getJSONArray("Featured")
            for (i in 0 until featureArray.length()) {
                featuredItems.add(gson.fromJson(featureArray.getJSONObject(i).toString(), Video::class.java))
            }

            val dailtContentArray = jsonObject.getJSONArray("DailyContent")
            for (i in 0 until dailtContentArray.length()) {
                dailyContentItems.add(gson.fromJson(dailtContentArray.getJSONObject(i).toString(), Video::class.java))
            }

            val classesArray = jsonObject.getJSONArray("Classes")
            for (i in 0 until classesArray.length()) {
                classesItems.add(gson.fromJson(classesArray.getJSONObject(i).toString(), Video::class.java))
            }

            val messagesArray = jsonObject.getJSONArray("Messages")
            for (i in 0 until messagesArray.length()) {
                messagesItems.add(gson.fromJson(messagesArray.getJSONObject(i).toString(), Video::class.java))
            }
            findViewById<RecyclerView>(R.id.top_recycler_view).adapter = FeatureAdapter(featuredItems,this,this)
            findViewById<RecyclerView>(R.id.featured_recyclerview).adapter = VideosAdapter(dailyContentItems,this,this,)
            findViewById<RecyclerView>(R.id.category_recyclerview).adapter = VideosAdapter(classesItems,this,this)
            findViewById<RecyclerView>(R.id.new_recyclerview).adapter = VideosAdapter(messagesItems,this,this)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        progressBar?.visibility = View.GONE
        mainView?.visibility = View.VISIBLE
    }

//    override fun onResponse(response: Any?) {
//        val videos = ArrayList<Video>()
//        try {
//            val jsonObject = JSONObject(response.toString())
//            val goalsArray = jsonObject.getJSONArray("videos")
//            val gson = Gson()
//            for (i in 0 until goalsArray.length()) {
//                videos.add(
//                    gson.fromJson(
//                        goalsArray.getJSONObject(i).toString(),
//                        Video::class.java
//                    )
//                )
//            }
//        } catch (e: JSONException) {
//            e.printStackTrace()
//        }
//        val videosAdapter = VideosAdapter(videos, this@VideosActivity)
//        val itemDecor = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
//        recyclerView!!.addItemDecoration(itemDecor)
//        recyclerView!!.adapter = videosAdapter
//        videosAdapter.videoItemListener = this
//        progressBar!!.visibility = View.GONE
//        recyclerView!!.visibility = View.VISIBLE
//    }

    override fun onVideoClick(video: Video) {
        val bundle = Bundle()
        bundle.putString(VIDEO_LINK,video.videoLink)
        startActivity(Intent(this,VideoPlayActivity::class.java).putExtras(bundle))
    }
}