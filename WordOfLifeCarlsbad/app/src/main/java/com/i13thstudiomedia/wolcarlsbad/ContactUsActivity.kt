package com.i13thstudiomedia.wolcarlsbad

import android.os.Bundle
import android.webkit.WebSettings
import android.webkit.WebView
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity


/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
class ContactUsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact_us)

        val myWebView: WebView = findViewById(R.id.webview)
        myWebView.loadUrl("https://bit.ly/ContactUSWOL2020App")
        val webSettings: WebSettings = myWebView.getSettings()
        webSettings.javaScriptEnabled = true

        findViewById<ImageView>(R.id.imageView4).setOnClickListener {
            finish()
        }

    }
}