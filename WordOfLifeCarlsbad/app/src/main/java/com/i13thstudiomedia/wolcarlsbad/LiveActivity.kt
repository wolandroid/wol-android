package com.i13thstudiomedia.wolcarlsbad

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.webkit.WebSettings
import android.webkit.WebView;
import android.widget.ImageView

class LiveActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_live)

        val myWebView: WebView = findViewById(R.id.webview)
        myWebView.loadUrl("https://cdn3.wowza.com/1/dnlVSmJSWFZmbitC/TG5PbmRs/hls/live/playlist.m3u8")
        val webSettings: WebSettings = myWebView.getSettings()
        webSettings.javaScriptEnabled = true

        findViewById<ImageView>(R.id.imageView3).setOnClickListener {
            finish()
        }

    }
}