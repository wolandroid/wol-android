package com.i13thstudiomedia.wolcarlsbad







import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageButton
import androidx.appcompat.app.AppCompatActivity
import com.i13thstudiomedia.wolcarlsbad.videos.VideosActivity


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val myButton = findViewById<View>(R.id.live) as ImageButton

        myButton.setOnClickListener { // there's an implicit view parameter as "it"
            // Intent is what you use to start another activity
            val intent = Intent(this, LiveActivity::class.java)
            startActivity(intent)
        }

        val myButton2 = findViewById<View>(R.id.imnew) as ImageButton

        myButton2.setOnClickListener { // there's an implicit view parameter as "it"
            // Intent is what you use to start another activity
            val intent = Intent(this, imnewActivity::class.java)
            startActivity(intent)
        }

        val myButton3 = findViewById<View>(R.id.listen) as ImageButton

        myButton3.setOnClickListener { // there's an implicit view parameter as "it"
            // Intent is what you use to start another activity
            val intent = Intent(this, ListenActivity::class.java)
            startActivity(intent)
        }

        val myButton4 = findViewById<View>(R.id.store) as ImageButton

        myButton4.setOnClickListener { // there's an implicit view parameter as "it"
            // Intent is what you use to start another activity
            val intent = Intent(this, StoreActivity::class.java)
            startActivity(intent)
        }

        val myButton5 = findViewById<View>(R.id.giving) as ImageButton

        myButton5.setOnClickListener { // there's an implicit view parameter as "it"
            // Intent is what you use to start another activity
            val intent = Intent(this, GivingActivity::class.java)
            startActivity(intent)
        }

        val myButton6 = findViewById<View>(R.id.involved) as ImageButton

        myButton6.setOnClickListener { // there's an implicit view parameter as "it"
            // Intent is what you use to start another activity
            val intent = Intent(this, GetInvolvedActivity::class.java)
            startActivity(intent)
        }

        val myButton7 = findViewById<View>(R.id.contact) as ImageButton

        myButton7.setOnClickListener { // there's an implicit view parameter as "it"
            // Intent is what you use to start another activity
            val intent = Intent(this, ContactUsActivity::class.java)
            startActivity(intent)
        }

        val wolDemandButton = findViewById<ImageButton>(R.id.wod).setOnClickListener {
            val intent = Intent(this, VideosActivity::class.java)
            startActivity(intent)
        }
    }
}
