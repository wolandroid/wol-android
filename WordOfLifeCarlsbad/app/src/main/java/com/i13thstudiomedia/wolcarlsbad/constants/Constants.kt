package com.i13thstudiomedia.wolcarlsbad.constants

class Constants {
    companion object{

        final val VIDEO_LINK = "videoLink"
        final val VIDEOS_URL = "https://wolvideos.firebaseapp.com/wolcontent.js"
    }
}