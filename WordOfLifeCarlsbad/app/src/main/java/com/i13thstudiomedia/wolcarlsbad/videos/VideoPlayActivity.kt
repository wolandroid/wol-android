package com.i13thstudiomedia.wolcarlsbad.videos

import android.net.Uri
import android.opengl.Visibility
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.MediaController
import android.widget.ProgressBar
import android.widget.VideoView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.VolleyError
import com.google.gson.Gson
import com.i13thstudiomedia.wolcarlsbad.R
import com.i13thstudiomedia.wolcarlsbad.constants.Constants.Companion.VIDEO_LINK
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class VideoPlayActivity : AppCompatActivity() {
    var videoView:VideoView ? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video_play)
        videoView = findViewById(R.id.videoView)
        var video = intent.extras?.getString(VIDEO_LINK)
        video?.let {
            videoView?.visibility = View.VISIBLE
            val mc = MediaController(this)
            mc.setAnchorView(videoView)
            mc.setMediaPlayer(videoView)
            val videoUri: Uri = Uri.parse(video)
            videoView?.setMediaController(mc)
            videoView?.setVideoURI(videoUri)
            videoView?.start()
        }
    }
}