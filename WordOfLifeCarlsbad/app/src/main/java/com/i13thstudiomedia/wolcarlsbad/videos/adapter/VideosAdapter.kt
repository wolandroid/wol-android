package com.i13thstudiomedia.wolcarlsbad.videos.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.i13thstudiomedia.wolcarlsbad.R
import com.i13thstudiomedia.wolcarlsbad.videos.model.Video
import java.util.*


class VideosAdapter(orderLines: ArrayList<Video>, context: Context,videoItemListener:VideoItemListener) : RecyclerView.Adapter<RecyclerView.ViewHolder?>() {
    var videos = ArrayList<Video>()
    var context: Context
    var videoItemListener:VideoItemListener? = null
        set(value) {
            field = value
        }
    class WTBoxViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
       // var about: TextView
        var name: TextView
        var cardImg: ImageView
        //var image: ImageView

        init {
            //about = itemView.findViewById(R.id.about)
            name = itemView.findViewById(R.id.title)
            cardImg = itemView.findViewById(R.id.cardImg)
//            image = itemView.findViewById(R.id.image)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var viewHolder: RecyclerView.ViewHolder? = null
        val layoutInflater = LayoutInflater.from(parent.context)
        val listItem: View = layoutInflater.inflate(R.layout.lay_item_video, parent, false)
        viewHolder = WTBoxViewHolder(listItem)
        return viewHolder
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val video = videos[position]
        val offerViewHolder = holder as WTBoxViewHolder
        offerViewHolder.name.text = video.name
        //offerViewHolder.about.text = video.description


        val requestOptions = RequestOptions()
        requestOptions.placeholder(R.drawable.placeholder)
        requestOptions.error(R.drawable.placeholder)

        Glide.with(context)
            .setDefaultRequestOptions(requestOptions)
            .load(video.thumbnail)
            .into(offerViewHolder.cardImg)
        holder.cardImg.setOnClickListener(View.OnClickListener {
            videoItemListener?.onVideoClick(video)
        })
    }

    override fun getItemCount(): Int {
        return videos.size
    }

    init {
        this.videos = orderLines
        this.context = context
        this.videoItemListener = videoItemListener
    }
    interface VideoItemListener{
        fun onVideoClick(video: Video);
    }
}