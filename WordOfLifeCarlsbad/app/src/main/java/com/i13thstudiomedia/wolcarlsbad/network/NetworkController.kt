package com.i13thstudiomedia.wolcarlsbad.network

import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.*
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley

class NetworkController(private val videosActivity: AppCompatActivity) {
    private var mRequestQueue: RequestQueue? = null

    interface NetworkResponseListener {
        fun onError(error: VolleyError?)
        fun onResponse(response: Any?)
    }

    val requestQueue: RequestQueue?
        get() {
            if (mRequestQueue == null) {
                mRequestQueue = Volley.newRequestQueue(videosActivity)
            }
            return mRequestQueue
        }

    fun addGetRequest(url: String?, responseListener: NetworkResponseListener) {
        addRequest(Request.Method.GET, url, null, responseListener)
    }

    fun addRequest(
        requestType: Int,
        url: String?,
        reqBody: String?,
        responseListener: NetworkResponseListener
    ) {
        val stringRequest: StringRequest = object : StringRequest(requestType, url,
            Response.Listener { response ->
                responseListener.onResponse(response)
                mRequestQueue?.cache?.clear()
            }, Response.ErrorListener { error ->
                println(error)
                responseListener.onError(error)
            mRequestQueue?.cache?.clear()
        }) {
            @Throws(AuthFailureError::class)
            override fun getBody(): ByteArray {
                return reqBody?.toByteArray() ?: super.getBody()
            }

            override fun getBodyContentType(): String {
                return CONTENT_TYPE_APP_JSON
            }
        }
        stringRequest.retryPolicy = DefaultRetryPolicy(
            REQUEST_TIMEOUT_MS,
            REQUEST_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        requestQueue!!.add(stringRequest)
    }

    companion object {
        const val REQUEST_TIMEOUT_MS = 10000 // 10 SECONDS
        const val REQUEST_RETRIES = 2
        const val CONTENT_TYPE_APP_JSON = "application/json"
    }
}