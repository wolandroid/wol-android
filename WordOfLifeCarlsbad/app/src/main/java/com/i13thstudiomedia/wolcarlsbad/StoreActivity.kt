package com.i13thstudiomedia.wolcarlsbad

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.webkit.WebSettings
import android.webkit.WebView
import android.widget.ImageView

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
class StoreActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_store)

        val myWebView: WebView = findViewById(R.id.webview)
        myWebView.loadUrl("https://word-of-life-church---cafe.square.site/shop/2")
        val webSettings: WebSettings = myWebView.getSettings()
        webSettings.javaScriptEnabled = true

        findViewById<ImageView>(R.id.imageView4).setOnClickListener {
            finish()
        }

    }
}